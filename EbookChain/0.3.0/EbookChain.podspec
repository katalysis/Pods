Pod::Spec.new do |s|
  s.name             = "EbookChain"
  s.version          = "0.3.0"
  s.summary          = "Blockchain interface for Ebook Lending app."
  s.description  = <<-DESC
                   Blockchain interface for Ebook Lending app. iOS only.
                   Written in Swift.
                   Copyright Katalysis 2016.
                   DESC
  s.homepage         = 'http://www.katalysis.io'
  s.license          = { :type => 'Proprietary', :file => 'LICENSE' }
  s.author           = { "Katalysis BV" => "info@katalysis.io" }
  s.source           = { :git => "https://gitlab.com/katalysis/EbookChain.git", :tag => s.version.to_s }
  s.platform = :ios
  s.ios.deployment_target = '10.0'
  s.requires_arc = true

  s.source_files = [
    'EbookChain/*.{h,swift}',
    'EbookChain/**/*.swift',
  ]

  s.ios.framework = 'UIKit'


  s.dependency 'EbookMessages', '~>0.3.1'
  s.dependency 'ErisKeys', '~>0.3.5'
  s.dependency 'JSONCodable', '~> 3.0.1'
  s.dependency 'Starscream', '~> 2.1.1'
  s.dependency 'ErisBlockchain', '0.1.4'
end
