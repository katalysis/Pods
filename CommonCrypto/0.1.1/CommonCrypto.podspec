Pod::Spec.new do |s|
  s.name             = "CommonCrypto"
  s.version          = "0.1.1"
  s.summary          = "CommonCrypto wrappers for Swift."
  s.description  = <<-DESC
		   CommonCrypto wrappers for Swift.
		   Taken from https://github.com/sergejp/CommonCrypto.git
		   Wrapped in to a CocoaPod by Katalysis.
                   DESC
  s.homepage         = 'https://github.com/sergejp/CommonCrypto'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { "Katalysis Holding" => "info@katalysis.io" }
  s.source           = { :git => "https://gitlab.com/katalysis/CommonCrypto.git", :tag => s.version.to_s }

  s.requires_arc = true

  s.source_files = [
    'CommonCrypto/*.modulemap',
  ]

end
