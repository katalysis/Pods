Pod::Spec.new do |s|
  s.name             = "BurrowBlockchain"
  s.version          = "0.4.0"
  s.summary          = "Blockchain websocket interface for Burrow blockchain."
  s.description  = <<-DESC
                   Blockchain websocket interface for Burrow blockchain. iOS only.
                   Written in Swift.
                   Copyright Katalysis 2016-2017.
                   DESC
  s.homepage         = 'http://www.katalysis.io'
  s.license          = { :type => 'Proprietary', :file => 'LICENSE' }
  s.author           = { "Katalysis BV" => "info@katalysis.io" }
  s.source           = { :git => "https://gitlab.com/katalysis/BurrowBlockchain.git", :tag => s.version.to_s }
  s.platform = :ios
  s.ios.deployment_target = '10.0'
  s.requires_arc = true

  s.source_files = [
    'Sources/*.{h,swift}',
    'Sources/**/*.swift',
  ]

  s.ios.framework = 'UIKit'

  s.dependency 'ErisKeys', '~>0.3.6'
  s.dependency 'JSONCodable', '~> 3.0.1'
  s.dependency 'Starscream', '~> 2.1.1'
end
