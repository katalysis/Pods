Pod::Spec.new do |s|
  s.name             = "EbookMessages"
  s.version          = "0.1.9"
  s.summary          = "Request/Response messages for Ebook Lending app."
  s.description  = <<-DESC
                   Request/Response messages for Ebook Lending app.
                   Written in Swift.
                   Copyright Katalysis 2016.
                   DESC
  s.homepage         = 'http://www.katalysis.io'
  s.license          = { :type => 'Proprietary', :file => 'LICENSE' }
  s.author           = { "Katalysis BV" => "info@katalysis.io" }
  s.source           = { :git => "https://gitlab.com/katalysis/EbookMessages.git", :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.10'
  s.requires_arc = true

  s.source_files = [
    'Sources/*.{h,swift}',
    'Sources/**/*.swift',
  ]

  s.dependency 'JSONCodable', '3.0.1'

end
