Pod::Spec.new do |s|
  s.name         = "Starscream"
  s.version      = "2.1.1"
  s.summary      = "A conforming WebSocket RFC 6455 client library in Swift for iOS and OSX, integrated with Zewo/Axis/Buffer"
s.homepage     = "https://gitlab.com/katalysis/Starscream"
  s.license      = 'Apache License, Version 2.0'
s.author       = {'Dalton Cherry' => 'http://daltoniam.com', 'Austin Cherry' => 'http://austincherry.me', 'Katalysis' =>  'http://katalysis.io', 'Zewo' => 'http://zewo.io'}
  s.source       = { :git => 'https://gitlab.com/katalysis/Starscream.git',  :tag => "#{s.version}"}
  s.social_media_url = 'http://twitter.com/katalysis_io'
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.10'
  s.tvos.deployment_target = '9.0'
  s.source_files = 'Source/*.swift'
  s.requires_arc = 'true'
end
