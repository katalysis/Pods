Pod::Spec.new do |s|
  s.name             = "ErisBlockchain"
  s.version          = "0.1.7"
  s.summary          = "Blockchain websocket interface for Eris blockchain."
  s.description  = <<-DESC
                   Blockchain websocket interface for Eris blockchain. iOS only.
                   Written in Swift.
                   Copyright Katalysis 2016.
                   DESC
  s.homepage         = 'http://www.katalysis.io'
  s.license          = { :type => 'Proprietary', :file => 'LICENSE' }
  s.author           = { "Katalysis BV" => "info@katalysis.io" }
  s.source           = { :git => "https://gitlab.com/katalysis/ErisBlockchain.git", :tag => s.version.to_s }
  s.platform = :ios
  s.ios.deployment_target = '10.0'
  s.requires_arc = true

  s.source_files = [
    'Sources/*.{h,swift}',
    'Sources/**/*.swift',
  ]

  s.ios.framework = 'UIKit'

  s.dependency 'ErisKeys', '~>0.3.5'
  s.dependency 'JSONCodable', '~> 3.0.1'
  s.dependency 'Starscream', '~> 2.1.1'
end
